import {computed, watch} from 'vue';
import * as yup from 'yup';
import {useField, useForm} from 'vee-validate';
import {useStore} from 'vuex';
import {useRouter} from 'vue-router';

export function useLoginForm() {
    const store = useStore()
    const router = useRouter()
    const {handleSubmit, isSubmitting, submitCount} = useForm()

    const {value: email, errorMessage: emailError, handleBlur: emailBlur} = useField(
        'email', 
        yup
            .string()
            .trim()
            .required('Введите email')
            .email('Введите коректный email')
    );

    const {value: password, errorMessage: passwordError, handleBlur: passwordBlur} = useField(
        'password',
        yup
            .string()
            .trim()
            .required('Введите пароль')
            .min(8, 'Пароль не может быть меньше 8 символов')
    );

    const isManiClick = computed(() => submitCount.value >= 2)

    watch(isManiClick, val => {
        if (val) {
            setTimeout(()=>  submitCount.value = 0, 1500)
        }
    })

    const onSubmit = handleSubmit(async values => {
        try {
            await store.dispatch('auth/login', values)
            router.push('/')   
        } catch (e) {}
    })

    return {
        email,
        password,
        emailError,
        passwordError,
        emailBlur,
        passwordBlur,
        onSubmit,
        isSubmitting,
        isManiClick
    }
}