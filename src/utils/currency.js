const formatter = new Intl.NumberFormat('ru-Ru', {currency: 'UAH', style: 'currency'});

export function currency(value) {
    return formatter.format(value)
}